import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:snapcard/helper/color_app/color_app.dart';
import 'package:snapcard/helper/hex_color/hex_color.dart';
import 'package:snapcard/pages/splashscreen/Splashscreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: Colors.black,
      systemNavigationBarIconBrightness: Brightness.light,
      systemNavigationBarDividerColor: Colors.white));
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MaterialApp(
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: HexColor("#FAFAFA"),
          scaffoldBackgroundColor: HexColor("#FFFFFF"),
          cardColor: HexColor("#FFFFFF"),
          tabBarTheme: TabBarTheme(
            unselectedLabelColor: Colors.grey,
          ),
          accentColor: ColorApp.main_color_app,
          hintColor: ColorApp.main_color_app,
        ),
        debugShowCheckedModeBanner: false,
        home: Splashscreen()));
  });
}
