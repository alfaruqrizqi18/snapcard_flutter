class ApiUrl {
  // static String base_url = "http://192.168.43.184/snapcard-api/index.php/";
  // static String base_url = "http://192.168.1.123/snapcard-api/index.php/";
  static String base_url =
      "https://snapcardtsel.000webhostapp.com/snapcard-api/index.php/";
  static String login = base_url + "users/login";
  static String create_sim_card = base_url + "sim/create_sim_card";
  static String create_sim_card_plus = base_url + "sim/create_sim_card_plus";
  static String create_bundling = base_url + "sim/create_bundling";
  static String check_whitelist = base_url + "sim/check_whitelist";
  static String check_blacklist = base_url + "sim/check_blacklist";
  static String report = base_url + "sim/report/";
}
