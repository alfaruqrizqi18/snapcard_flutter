import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:snapcard/helper/color_app/color_app.dart';
import 'package:snapcard/helper/font_setting/font_setting.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:snapcard/helper/url/url.dart';

class SimCardForm extends StatefulWidget {
  @override
  _SimCardFormState createState() => _SimCardFormState();
}

class _SimCardFormState extends State<SimCardForm> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<Map> data_sim = [];
  int data_sim_success = 0;
  int data_sim_failed = 0;

  Response response;
  Dio dio = new Dio();
  Future store() async {
    for (int i = 0; i < data_sim.length; i++) {
      print(data_sim[i]);
      response = await dio.post(ApiUrl.create_sim_card,
          data:
              FormData.fromMap({"sim_card_barcode": data_sim[i]['subtitle']}));
      print("${i} -> ${response}");
      if (response.data['message'] == "success") {
        setState(() {
          data_sim[i]['status'] = "sent";
          data_sim_success++;
        });
      } else if (response.data['message'] == "duplicate") {
        setState(() {
          data_sim[i]['status'] = "duplicate";
          data_sim_failed++;
        });
      }
    }
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.green[400],
      elevation: 0,
      duration: Duration(milliseconds: 3000),
      behavior: SnackBarBehavior.fixed,
      content: Text(
        "${data_sim_success} data berhasil di kirim",
        style: TextStyle(
          fontFamily: FontSetting.fontMain,
        ),
      ),
    ));
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.red[400],
      elevation: 0,
      duration: Duration(milliseconds: 3000),
      behavior: SnackBarBehavior.fixed,
      content: Text(
        "${data_sim_failed} data gagal di kirim",
        style: TextStyle(
          fontFamily: FontSetting.fontMain,
        ),
      ),
    ));
    setState(() {
      data_sim_success = 0;
      data_sim_failed = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text("SIM Card",
            style: TextStyle(
                color: Colors.grey[300],
                fontFamily: FontSetting.fontMain,
                fontSize: 12,
                fontWeight: FontWeight.w700)),
        actions: <Widget>[
          data_sim.isEmpty
              ? Opacity(opacity: 0)
              : GestureDetector(
                  onTap: () {
                    store();
                  },
                  child: Container(
                    padding:
                        EdgeInsets.only(right: 10, left: 10, top: 0, bottom: 0),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: ColorApp.main_color_app.withOpacity(0.2)),
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(right: 20.0, top: 10.0, bottom: 10),
                    child: Text("Kirim",
                        style: TextStyle(
                            color: ColorApp.main_color_app,
                            fontFamily: FontSetting.fontMain,
                            fontSize: 12,
                            fontWeight: FontWeight.w700)),
                  ),
                )
        ],
      ),
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: 15, right: 15),
        child: FloatingActionButton(
          onPressed: () async {
            String cameraScanResult = await scanner.scan();
            if (cameraScanResult.toString().isNotEmpty) {
              setState(() {
                data_sim.add({
                  "title": DateTime.now(),
                  "status": "queued",
                  "subtitle": cameraScanResult.toString(),
                });
              });
            }
          },
          backgroundColor: ColorApp.main_color_app,
          child: Icon(Icons.center_focus_weak),
        ),
      ),
      body: data_sim.isEmpty
          ? Center(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  "Belum ada data yang kamu masukkan",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.grey[400],
                      fontFamily: FontSetting.fontMain,
                      fontSize: 18),
                ),
              ),
            )
          : ListView(
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height * 0.15),
              children: <Widget>[
                Container(
                  child: ListTile(
                    title: Text("Antrian",
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: FontSetting.fontMain,
                            fontWeight: FontWeight.bold,
                            fontSize: 20)),
                    subtitle: Text(
                        "List antrian barcode yang akan di kirim ke server database",
                        style: TextStyle(
                            color: Colors.black45,
                            fontFamily: FontSetting.fontMain,
                            fontWeight: FontWeight.normal,
                            fontSize: 13)),
                    trailing: GestureDetector(
                      onTap: () {
                        setState(() {
                          data_sim.clear();
                        });
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.2,
                        padding: EdgeInsets.only(
                            right: 10, left: 10, top: 0, bottom: 0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.green[400].withOpacity(0.2)),
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(top: 10.0, bottom: 10),
                        child: Text("Reset",
                            style: TextStyle(
                                color: Colors.green[400],
                                fontFamily: FontSetting.fontMain,
                                fontSize: 12,
                                fontWeight: FontWeight.w700)),
                      ),
                    ),
                  ),
                ),
                ListView.builder(
                    shrinkWrap: true,
                    physics: BouncingScrollPhysics(),
                    itemCount: data_sim.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        leading: CircleAvatar(
                          backgroundColor: Colors.grey[100],
                          child: Text(
                            (index + 1).toString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.bold,
                                fontFamily: FontSetting.fontMain,
                                fontSize: 14),
                          ),
                        ),
                        title: Text(
                          "${DateFormat('dd MMM yyyy').format(data_sim[index]['title'])} - ${data_sim[index]['status'] == "sent" ? "Telah terkirim" : data_sim[index]['status'] == "queued" ? "Mengantri" : "Terjadi duplikasi"}",
                          style: TextStyle(
                              color: Colors.grey[400],
                              fontFamily: FontSetting.fontMain,
                              fontSize: 12),
                        ),
                        subtitle: Text(data_sim[index]['subtitle'],
                            style: TextStyle(
                                color: data_sim[index]['status'] == "sent"
                                    ? Colors.green[400]
                                    : data_sim[index]['status'] == "queued"
                                        ? Colors.black87
                                        : Colors.red[400],
                                fontFamily: FontSetting.fontMain,
                                fontWeight: FontWeight.bold,
                                fontSize: 15)),
                        trailing: GestureDetector(
                          onTap: () {
                            setState(() {
                              data_sim.removeAt(index);
                            });
                          },
                          child: Container(
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                color: Colors.grey[100],
                                borderRadius: BorderRadius.circular(8)),
                            child: Icon(
                              Icons.delete,
                              size: 20,
                              color: ColorApp.main_color_app.withOpacity(0.8),
                            ),
                          ),
                        ),
                      );
                    })
              ],
            ),
    );
  }
}
