import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:snapcard/helper/color_app/color_app.dart';
import 'package:snapcard/helper/font_setting/font_setting.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:snapcard/helper/url/url.dart';

class BundlingForm extends StatefulWidget {
  @override
  _BundlingFormState createState() => _BundlingFormState();
}

class _BundlingFormState extends State<BundlingForm> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController sim_card_1 = TextEditingController();
  TextEditingController sim_card_2 = TextEditingController();
  TextEditingController imei_1 = TextEditingController();
  TextEditingController imei_2 = TextEditingController();
  String brand;
  List brands = [
    'Samsung',
    'Oppo',
    'Vivo',
    'Apple',
    'Advan',
    'Evercross',
    'Nokia',
    'Motorola',
    'Xiaomi',
    'Huawei',
    'Asus',
    'Lainnya',
  ];

  Response response;
  Dio dio = new Dio();
  Future store() async {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.blue[400],
      elevation: 0,
      duration: Duration(milliseconds: 2000),
      behavior: SnackBarBehavior.fixed,
      content: Text(
        "Sedang mengirim...",
        style: TextStyle(
          fontFamily: FontSetting.fontMain,
        ),
      ),
    ));
    response = await dio.post(ApiUrl.create_bundling,
        data: FormData.fromMap({
          "sim_card_1": sim_card_1.text.toString(),
          "sim_card_2": sim_card_2.text.toString(),
          "imei_1": imei_1.text.toString(),
          "imei_2": imei_2.text.toString(),
          "brand": brand.toString()
        }));
    print(response);
    if (response.data['message'] == "success") {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.green[400],
        elevation: 0,
        duration: Duration(milliseconds: 3000),
        behavior: SnackBarBehavior.fixed,
        content: Text(
          "Data berhasil di kirim",
          style: TextStyle(
            fontFamily: FontSetting.fontMain,
          ),
        ),
      ));
      setState(() {
        sim_card_1.clear();
        sim_card_2.clear();
        imei_1.clear();
        imei_2.clear();
        brand = null;
      });
    } else if (response.data['message'] == "duplicate") {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.red[400],
        elevation: 0,
        duration: Duration(milliseconds: 3000),
        behavior: SnackBarBehavior.fixed,
        content: Text(
          "Data gagal terkirim karena terjadi duplikasi",
          style: TextStyle(
            fontFamily: FontSetting.fontMain,
          ),
        ),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text("Bundling",
            style: TextStyle(
                color: Colors.grey[300],
                fontFamily: FontSetting.fontMain,
                fontSize: 12,
                fontWeight: FontWeight.w700)),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              if (sim_card_1.text.isNotEmpty &&
                  sim_card_2.text.isNotEmpty &&
                  imei_1.text.isNotEmpty &&
                  imei_2.text.isNotEmpty &&
                  brand != null) {
                store();
              } else {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  backgroundColor: Colors.red[400],
                  elevation: 0,
                  duration: Duration(milliseconds: 3000),
                  behavior: SnackBarBehavior.fixed,
                  content: Text(
                    "Mohon isi semua form",
                    style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                    ),
                  ),
                ));
              }
            },
            child: Container(
              padding: EdgeInsets.only(right: 10, left: 10, top: 0, bottom: 0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: ColorApp.main_color_app.withOpacity(0.2)),
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 20.0, top: 10.0, bottom: 10),
              child: Text("Kirim",
                  style: TextStyle(
                      color: ColorApp.main_color_app,
                      fontFamily: FontSetting.fontMain,
                      fontSize: 12,
                      fontWeight: FontWeight.w700)),
            ),
          )
        ],
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.15),
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 20.0, right: 15.0, left: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "SIM Card 1",
                  style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                      color: Colors.black87,
                      fontWeight: FontWeight.w800,
                      fontSize: 13.0),
                ),
                Container(
                    transform: Matrix4.translationValues(0.0, -2.0, 0.0),
                    child: Theme(
                        child: TextFormField(
                          controller: sim_card_1,
                          keyboardType: TextInputType.text,
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          cursorColor: ColorApp.main_color_app,
                          style: TextStyle(
                              fontFamily: FontSetting.fontMain,
                              color: Colors.black54,
                              fontSize: 15),
                          obscureText: false,
                          decoration: InputDecoration(
                              suffixIcon: GestureDetector(
                                onTap: () async {
                                  String cameraScanResult =
                                      await scanner.scan();
                                  if (cameraScanResult.toString().isNotEmpty) {
                                    setState(() {
                                      sim_card_1.text =
                                          cameraScanResult.toString();
                                    });
                                  }
                                },
                                child: Icon(
                                  Icons.center_focus_weak,
                                  size: 20,
                                ),
                              ),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.black12)),
                              focusedBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: ColorApp.main_color_app)),
                              hintText: "SIM Card 1...",
                              hintStyle: TextStyle(
                                  fontFamily: FontSetting.fontMain,
                                  color: Colors.grey)),
                        ),
                        data: Theme.of(context).copyWith(
                          primaryColor: ColorApp.main_color_app,
                        ))),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20.0, right: 15.0, left: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "SIM Card 2",
                  style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                      color: Colors.black87,
                      fontWeight: FontWeight.w800,
                      fontSize: 13.0),
                ),
                Container(
                    transform: Matrix4.translationValues(0.0, -2.0, 0.0),
                    child: Theme(
                        child: TextFormField(
                          controller: sim_card_2,
                          keyboardType: TextInputType.text,
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          cursorColor: ColorApp.main_color_app,
                          style: TextStyle(
                              fontFamily: FontSetting.fontMain,
                              color: Colors.black54,
                              fontSize: 15),
                          obscureText: false,
                          decoration: InputDecoration(
                              suffixIcon: GestureDetector(
                                onTap: () async {
                                  String cameraScanResult =
                                      await scanner.scan();
                                  if (cameraScanResult.toString().isNotEmpty) {
                                    setState(() {
                                      sim_card_2.text =
                                          cameraScanResult.toString();
                                    });
                                  }
                                },
                                child: Icon(
                                  Icons.center_focus_weak,
                                  size: 20,
                                ),
                              ),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.black12)),
                              focusedBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: ColorApp.main_color_app)),
                              hintText: "SIM Card 2...",
                              hintStyle: TextStyle(
                                  fontFamily: FontSetting.fontMain,
                                  color: Colors.grey)),
                        ),
                        data: Theme.of(context).copyWith(
                          primaryColor: ColorApp.main_color_app,
                        ))),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20.0, right: 15.0, left: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "IMEI 1",
                  style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                      color: Colors.black87,
                      fontWeight: FontWeight.w800,
                      fontSize: 13.0),
                ),
                Container(
                    transform: Matrix4.translationValues(0.0, -2.0, 0.0),
                    child: Theme(
                        child: TextFormField(
                          controller: imei_1,
                          keyboardType: TextInputType.text,
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          cursorColor: ColorApp.main_color_app,
                          style: TextStyle(
                              fontFamily: FontSetting.fontMain,
                              color: Colors.black54,
                              fontSize: 15),
                          obscureText: false,
                          decoration: InputDecoration(
                              suffixIcon: GestureDetector(
                                onTap: () async {
                                  String cameraScanResult =
                                      await scanner.scan();
                                  if (cameraScanResult.toString().isNotEmpty) {
                                    setState(() {
                                      imei_1.text = cameraScanResult.toString();
                                    });
                                  }
                                },
                                child: Icon(
                                  Icons.center_focus_weak,
                                  size: 20,
                                ),
                              ),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.black12)),
                              focusedBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: ColorApp.main_color_app)),
                              hintText: "IMEI 1...",
                              hintStyle: TextStyle(
                                  fontFamily: FontSetting.fontMain,
                                  color: Colors.grey)),
                        ),
                        data: Theme.of(context).copyWith(
                          primaryColor: ColorApp.main_color_app,
                        ))),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20.0, right: 15.0, left: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "IMEI 2",
                  style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                      color: Colors.black87,
                      fontWeight: FontWeight.w800,
                      fontSize: 13.0),
                ),
                Container(
                    transform: Matrix4.translationValues(0.0, -2.0, 0.0),
                    child: Theme(
                        child: TextFormField(
                          controller: imei_2,
                          keyboardType: TextInputType.text,
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          cursorColor: ColorApp.main_color_app,
                          style: TextStyle(
                              fontFamily: FontSetting.fontMain,
                              color: Colors.black54,
                              fontSize: 15),
                          obscureText: false,
                          decoration: InputDecoration(
                              suffixIcon: GestureDetector(
                                onTap: () async {
                                  String cameraScanResult =
                                      await scanner.scan();
                                  if (cameraScanResult.toString().isNotEmpty) {
                                    setState(() {
                                      imei_2.text = cameraScanResult.toString();
                                    });
                                  }
                                },
                                child: Icon(
                                  Icons.center_focus_weak,
                                  size: 20,
                                ),
                              ),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.black12)),
                              focusedBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: ColorApp.main_color_app)),
                              hintText: "IMEI 2...",
                              hintStyle: TextStyle(
                                  fontFamily: FontSetting.fontMain,
                                  color: Colors.grey)),
                        ),
                        data: Theme.of(context).copyWith(
                          primaryColor: ColorApp.main_color_app,
                        ))),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20.0, right: 15.0, left: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Brand",
                  style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                      color: Colors.black87,
                      fontWeight: FontWeight.w800,
                      fontSize: 13.0),
                ),
                Container(
                    transform: Matrix4.translationValues(0.0, -2.0, 0.0),
                    child: DropdownButton(
                      elevation: 0,
                      icon: Container(
                        margin: EdgeInsets.only(right: 15.0),
                        child: Icon(
                          Icons.branding_watermark,
                          color: Colors.grey[400],
                          size: 20.0,
                        ),
                      ),
                      isExpanded: true,
                      hint: Text(
                        "Pilih brand",
                        style: TextStyle(
                            fontFamily: FontSetting.fontMain,
                            color: Colors.black87,
                            fontWeight: FontWeight.w500,
                            fontSize: 14.0),
                      ),
                      value: brand,
                      items: brands.map((value) {
                        return DropdownMenuItem(
                          child: Text(
                            value,
                            style: TextStyle(
                                fontFamily: FontSetting.fontMain,
                                color: Colors.black87,
                                fontWeight: FontWeight.w500,
                                fontSize: 14.0),
                          ),
                          value: value,
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          brand = value;
                        });
                      },
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }
}
