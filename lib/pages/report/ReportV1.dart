import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:snapcard/helper/color_app/color_app.dart';
import 'package:snapcard/helper/font_setting/font_setting.dart';
import 'package:snapcard/helper/url/url.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

class ReportV1 extends StatefulWidget {
  @override
  _ReportV1State createState() => _ReportV1State();
}

class _ReportV1State extends State<ReportV1> {
  String current_chip = "sim_card";
  List data_sim_card = [];
  List data_sim_card_plus = [];
  List data_bundling = [];
  DateTime val_start = DateTime.now().add(Duration(days: -7));
  DateTime val_end = new DateTime.now();
  String start = "";
  String end = "";
  Response response;
  Dio dio = new Dio();

  Future check_report() async {
    response = await dio
        .get(ApiUrl.report, queryParameters: {"start": start, "end": end});
    print(response);
    setState(() {
      data_sim_card = response.data['sim_card']['data'] == null
          ? []
          : response.data['sim_card']['data'];
      data_sim_card_plus = response.data['sim_card_plus']['data'] == null
          ? []
          : response.data['sim_card_plus']['data'];
      data_bundling = response.data['bundling']['data'] == null
          ? []
          : response.data['bundling']['data'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      start = DateFormat("yyyy-M-dd").format(val_end.add(Duration(days: -7)));
      end = DateFormat("yyyy-M-dd").format(val_end);
      print(start);
    });
    check_report();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text("Report",
            style: TextStyle(
                color: Colors.grey[300],
                fontFamily: FontSetting.fontMain,
                fontSize: 12,
                fontWeight: FontWeight.w700)),
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          ListTile(
              onTap: () async {
                final List<DateTime> picked =
                    await DateRagePicker.showDatePicker(
                        context: context,
                        initialFirstDate: val_start,
                        initialLastDate: val_end,
                        firstDate: new DateTime(2020),
                        lastDate: new DateTime(2030));
                if (picked != null && picked.length == 2) {
                  print(picked[0]);
                  print(picked[1]);
                  setState(() {
                    start = DateFormat("yyyy-M-dd").format(picked[0]);
                    val_start = picked[0];
                    end = DateFormat("yyyy-M-dd").format(picked[1]);
                    val_end = picked[1];
                  });
                  print(start);
                  print(end);
                  check_report();
                }
              },
              title: Text("Rentang Tanggal",
                  style: TextStyle(
                      color: Colors.black54,
                      fontFamily: FontSetting.fontMain,
                      fontSize: 17,
                      fontWeight: FontWeight.w700)),
              subtitle: Text(
                  "${DateFormat("dd MMM yyyy").format(val_start)} - ${DateFormat("dd MMM yyyy").format(val_end)}",
                  style: TextStyle(
                      color: Colors.grey,
                      fontFamily: FontSetting.fontMain,
                      fontSize: 12,
                      fontWeight: FontWeight.w500)),
              trailing: Icon(Icons.calendar_today,
                  size: 22, color: Colors.grey[300])),
          Container(
            height: MediaQuery.of(context).size.height * 0.12,
            // color: Colors.red,
            child: ListView(
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.only(left: 15),
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 10.0),
                  child: InputChip(
                    backgroundColor: current_chip == "sim_card"
                        ? ColorApp.main_color_app.withOpacity(0.2)
                        : Colors.white,
                    onPressed: () {
                      setState(() {
                        current_chip = "sim_card";
                      });
                    },
                    avatar: CircleAvatar(
                      backgroundColor: current_chip == "sim_card"
                          ? ColorApp.main_color_app
                          : Colors.grey[300],
                      child: Text(""),
                    ),
                    label: Text(
                      'SIM Card',
                      style: TextStyle(
                          fontFamily: FontSetting.fontMain,
                          fontWeight: current_chip == "sim_card"
                              ? FontWeight.bold
                              : FontWeight.w500,
                          color: current_chip == "sim_card"
                              ? ColorApp.main_color_app
                              : Colors.grey[300]),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 5.0),
                  child: InputChip(
                    backgroundColor: current_chip == "sim_card_plus"
                        ? ColorApp.main_color_app.withOpacity(0.2)
                        : Colors.white,
                    onPressed: () {
                      setState(() {
                        current_chip = "sim_card_plus";
                      });
                    },
                    avatar: CircleAvatar(
                      backgroundColor: current_chip == "sim_card_plus"
                          ? ColorApp.main_color_app
                          : Colors.grey[300],
                      child: Text(""),
                    ),
                    label: Text(
                      'SIM Card+',
                      style: TextStyle(
                          fontFamily: FontSetting.fontMain,
                          fontWeight: current_chip == "sim_card_plus"
                              ? FontWeight.bold
                              : FontWeight.w500,
                          color: current_chip == "sim_card_plus"
                              ? ColorApp.main_color_app
                              : Colors.grey[300]),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 10.0),
                  child: InputChip(
                    backgroundColor: current_chip == "bundling"
                        ? ColorApp.main_color_app.withOpacity(0.2)
                        : Colors.white,
                    onPressed: () {
                      setState(() {
                        current_chip = "bundling";
                      });
                    },
                    avatar: CircleAvatar(
                      backgroundColor: current_chip == "bundling"
                          ? ColorApp.main_color_app
                          : Colors.grey[300],
                      child: Text(""),
                    ),
                    label: Text(
                      'Bundling',
                      style: TextStyle(
                          fontFamily: FontSetting.fontMain,
                          fontWeight: current_chip == "bundling"
                              ? FontWeight.bold
                              : FontWeight.w500,
                          color: current_chip == "bundling"
                              ? ColorApp.main_color_app
                              : Colors.grey[300]),
                    ),
                  ),
                )
              ],
            ),
          ),
          current_chip == "sim_card"
              ? ListView.builder(
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  itemCount: data_sim_card.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.grey[100],
                        child: Text(
                          (index + 1).toString(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold,
                              fontFamily: FontSetting.fontMain,
                              fontSize: 14),
                        ),
                      ),
                      title: Text(
                        data_sim_card[index]['sim_card_barcode'].toString(),
                        style: TextStyle(
                            fontFamily: FontSetting.fontMain,
                            color: Colors.black54,
                            fontWeight: FontWeight.w500,
                            fontSize: 13.0),
                      ),
                      subtitle: Text(
                        DateFormat("dd MMM yyyy").format(
                            DateTime.parse(data_sim_card[index]['created_at'])),
                        style: TextStyle(
                            fontFamily: FontSetting.fontMain,
                            color: Colors.black54,
                            fontWeight: FontWeight.w500,
                            fontSize: 13.0),
                      ),
                    );
                  },
                )
              : current_chip == "sim_card_plus"
                  ? ListView.builder(
                      shrinkWrap: true,
                      physics: BouncingScrollPhysics(),
                      itemCount: data_sim_card_plus.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          leading: CircleAvatar(
                            backgroundColor: Colors.grey[100],
                            child: Text(
                              (index + 1).toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: FontSetting.fontMain,
                                  fontSize: 14),
                            ),
                          ),
                          title: Text(
                            data_sim_card_plus[index]['sim_card_plus_barcode']
                                .toString(),
                            style: TextStyle(
                                fontFamily: FontSetting.fontMain,
                                color: Colors.black54,
                                fontWeight: FontWeight.w500,
                                fontSize: 13.0),
                          ),
                          subtitle: Text(
                            data_sim_card_plus[index]['physical_voucher'],
                            style: TextStyle(
                                fontFamily: FontSetting.fontMain,
                                color: Colors.black54,
                                fontWeight: FontWeight.w500,
                                fontSize: 13.0),
                          ),
                          trailing: Text(
                            DateFormat("dd MMM yyyy").format(DateTime.parse(
                                data_sim_card_plus[index]['created_at'])),
                            style: TextStyle(
                                fontFamily: FontSetting.fontMain,
                                color: Colors.black54,
                                fontWeight: FontWeight.w500,
                                fontSize: 10.0),
                          ),
                        );
                      },
                    )
                  : ListView.separated(
                      shrinkWrap: true,
                      physics: BouncingScrollPhysics(),
                      itemCount: data_bundling.length,
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                      itemBuilder: (context, index) {
                        return Container(
                          margin: EdgeInsets.only(bottom: 10, top: 10),
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                leading: CircleAvatar(
                                  backgroundColor: Colors.grey[100],
                                  child: Text(
                                    (index + 1).toString(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: FontSetting.fontMain,
                                        fontSize: 14),
                                  ),
                                ),
                                title: Text(
                                  "SIM 1 : ${data_bundling[index]['sim_card_1']}\nIMEI 1 : ${data_bundling[index]['imei_1']}"
                                      .toString(),
                                  style: TextStyle(
                                      fontFamily: FontSetting.fontMain,
                                      color: Colors.black54,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 13.0),
                                ),
                                subtitle: Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Text(
                                    "SIM 2 : ${data_bundling[index]['sim_card_2']}\nIMEI 2 : ${data_bundling[index]['imei_2']}",
                                    style: TextStyle(
                                        fontFamily: FontSetting.fontMain,
                                        color: Colors.black54,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 13.0),
                                  ),
                                ),
                                trailing: Text(
                                  "${data_bundling[index]['brand']}\n${DateFormat("dd MMM yyyy").format(DateTime.parse(data_bundling[index]['created_at']))}",
                                  style: TextStyle(
                                      fontFamily: FontSetting.fontMain,
                                      color: Colors.black54,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 10.0),
                                ),
                              )
                            ],
                          ),
                        );
                      },
                    )
        ],
      ),
    );
  }
}
