import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:snapcard/helper/color_app/color_app.dart';
import 'package:snapcard/helper/font_setting/font_setting.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:snapcard/helper/url/url.dart';

class WhitelistForm extends StatefulWidget {
  @override
  _WhitelistFormState createState() => _WhitelistFormState();
}

class _WhitelistFormState extends State<WhitelistForm> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController phone_number_or_imei = TextEditingController();

  Response response;
  Dio dio = new Dio();
  Future check() async {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.blue[400],
      elevation: 0,
      duration: Duration(milliseconds: 2000),
      behavior: SnackBarBehavior.fixed,
      content: Text(
        "Mencoba mencari di database...",
        style: TextStyle(
          fontFamily: FontSetting.fontMain,
        ),
      ),
    ));
    response = await dio.post(ApiUrl.check_whitelist,
        data: FormData.fromMap({
          "input_phone_number_or_imei": phone_number_or_imei.text.toString(),
        }));
    if (response.data['message'] == "success") {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.green[400],
        elevation: 0,
        duration: Duration(milliseconds: 3000),
        behavior: SnackBarBehavior.fixed,
        content: Text(
          "Data tersedia di dalam database",
          style: TextStyle(
            fontFamily: FontSetting.fontMain,
          ),
        ),
      ));
    } else if (response.data['message'] == "not_found") {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.red[400],
        elevation: 0,
        duration: Duration(milliseconds: 3000),
        behavior: SnackBarBehavior.fixed,
        content: Text(
          "Data tidak tersedia di database",
          style: TextStyle(
            fontFamily: FontSetting.fontMain,
          ),
        ),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text("Whitelist",
            style: TextStyle(
                color: Colors.grey[300],
                fontFamily: FontSetting.fontMain,
                fontSize: 12,
                fontWeight: FontWeight.w700)),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              if (phone_number_or_imei.text.isNotEmpty) {
                check();
              } else {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  backgroundColor: Colors.red[400],
                  elevation: 0,
                  duration: Duration(milliseconds: 3000),
                  behavior: SnackBarBehavior.fixed,
                  content: Text(
                    "Mohon isi semua form",
                    style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                    ),
                  ),
                ));
              }
            },
            child: Container(
              padding: EdgeInsets.only(right: 10, left: 10, top: 0, bottom: 0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: ColorApp.main_color_app.withOpacity(0.2)),
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 20.0, top: 10.0, bottom: 10),
              child: Text("Cari",
                  style: TextStyle(
                      color: ColorApp.main_color_app,
                      fontFamily: FontSetting.fontMain,
                      fontSize: 12,
                      fontWeight: FontWeight.w700)),
            ),
          )
        ],
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.15),
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 20.0, right: 15.0, left: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Nomor atau IMEI",
                  style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                      color: Colors.black87,
                      fontWeight: FontWeight.w800,
                      fontSize: 13.0),
                ),
                Container(
                    transform: Matrix4.translationValues(0.0, -2.0, 0.0),
                    child: Theme(
                        child: TextFormField(
                          controller: phone_number_or_imei,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          cursorColor: ColorApp.main_color_app,
                          style: TextStyle(
                              fontFamily: FontSetting.fontMain,
                              color: Colors.black54,
                              fontSize: 15),
                          obscureText: false,
                          decoration: InputDecoration(
                              suffixIcon: GestureDetector(
                                onTap: () async {
                                  String cameraScanResult =
                                      await scanner.scan();
                                  if (cameraScanResult.toString().isNotEmpty) {
                                    setState(() {
                                      phone_number_or_imei.text =
                                          cameraScanResult.toString();
                                    });
                                  }
                                },
                                child: Icon(
                                  Icons.center_focus_weak,
                                  size: 20,
                                ),
                              ),
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.black12)),
                              focusedBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: ColorApp.main_color_app)),
                              hintText: "Nomor atau IMEI...",
                              hintStyle: TextStyle(
                                  fontFamily: FontSetting.fontMain,
                                  color: Colors.grey)),
                        ),
                        data: Theme.of(context).copyWith(
                          primaryColor: ColorApp.main_color_app,
                        ))),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
