import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:snapcard/helper/route_shortcut/route_shortcut.dart';
import 'package:snapcard/helper/shared_preference_key/shared_preference_key.dart';
import 'package:snapcard/pages/auth/SignIn.dart';
import 'package:snapcard/pages/mainpage.dart';

class Splashscreen extends StatefulWidget {
  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  bool is_logged_in = false;
  bool is_guest = false;
  String token = "";
  SharedPreferences prefs;
  get_status_login() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      if (prefs.getBool(SPKey.is_logged_in) != null) {
        if (prefs.getBool(SPKey.is_logged_in)) {
          is_logged_in = true;
        } else {
          is_logged_in = false;
        }
      }
      if (prefs.getBool(SPKey.is_guest) != null) {
        if (prefs.getBool(SPKey.is_guest)) {
          is_guest = true;
        } else {
          is_guest = false;
        }
      }
    });
  }

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
    get_status_login();
    timeout();
  }

  Future<Timer> timeout() async {
    return new Timer(Duration(milliseconds: 2000), on_done_loading);
  }

  on_done_loading() async {
    is_logged_in || is_guest ? go_to_main_page() : go_to_auth_page();
  }

  go_to_auth_page() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    RouteShortcut().PushReplacement(context, SignIn());
  }

  go_to_main_page() {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    RouteShortcut().PushReplacement(context, MainPage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: RichText(
            text: TextSpan(
                text: "Snap",
                style: GoogleFonts.josefinSans(
                    fontSize: 24,
                    color: Colors.red[300],
                    fontWeight: FontWeight.w900),
                children: <TextSpan>[
              TextSpan(
                  text: "card",
                  style: GoogleFonts.josefinSans(
                      fontSize: 24,
                      color: Colors.grey[300],
                      fontWeight: FontWeight.w900))
            ])),
      ),
    );
  }
}
