import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:snapcard/helper/font_setting/font_setting.dart';
import 'package:snapcard/helper/hex_color/hex_color.dart';
import 'package:snapcard/helper/route_shortcut/route_shortcut.dart';
import 'package:snapcard/pages/auth/SignIn.dart';
import 'package:snapcard/pages/bundling/BundlingForm.dart';
import 'package:snapcard/pages/report/ReportV1.dart';
import 'package:snapcard/pages/sim_card/SimCardForm.dart';
import 'package:snapcard/pages/blacklist/BlacklistForm.dart';
import 'package:snapcard/pages/whitelist/WhitelistForm.dart';
import 'package:snapcard/pages/sim_card_plus/SimCardPlusForm.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text("HOMEPAGE",
            style: TextStyle(
                color: Colors.grey[300],
                fontFamily: FontSetting.fontMain,
                fontSize: 12,
                fontWeight: FontWeight.w700)),
        actions: <Widget>[
          GestureDetector(
            onTap: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.clear();
              RouteShortcut().PushReplacement(context, SignIn());
            },
            child: Container(
              width: MediaQuery.of(context).size.width * 0.2,
              padding: EdgeInsets.only(right: 10, left: 10, top: 0, bottom: 0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.red[400].withOpacity(0.2)),
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 10.0, bottom: 10, right: 20.0),
              child: Text("Logout",
                  style: TextStyle(
                      color: Colors.red[400],
                      fontFamily: FontSetting.fontMain,
                      fontSize: 12,
                      fontWeight: FontWeight.w700)),
            ),
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 10.0, bottom: 0.0),
            child: ListTile(
              title: Text("Hai,",
                  style: TextStyle(
                      color: Colors.black87,
                      fontFamily: FontSetting.fontMain,
                      fontWeight: FontWeight.w900,
                      fontSize: 27)),
              subtitle: Text("Selamat datang di aplikasi Snapcard",
                  style: TextStyle(
                      color: Colors.black45,
                      fontFamily: FontSetting.fontMain,
                      fontWeight: FontWeight.normal,
                      fontSize: 13)),
              trailing: Text(DateFormat("dd MMM yyyy").format(DateTime.now()),
                  style: TextStyle(
                      color: Colors.grey[400],
                      fontFamily: FontSetting.fontMain,
                      fontSize: 12,
                      fontWeight: FontWeight.w600)),
            ),
          ),
          GridView(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 10.0, bottom: 0.0),
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4,
              childAspectRatio: 0.75,
            ),
            children: <Widget>[
              buildUpperMenu(HexColor("#ffffff"),
                  "assets/images/icon/sim_card.png", "Sim Card", () {
                RouteShortcut().Push(context, SimCardForm());
              }),
              buildUpperMenu(HexColor("#ffffff"),
                  "assets/images/icon/sim_card.png", "Sim Card+", () {
                RouteShortcut().Push(context, SimCardPlusForm());
              }),
              buildUpperMenu(HexColor("#ffffff"),
                  "assets/images/icon/bundle.png", "Bundling", () {
                RouteShortcut().Push(context, BundlingForm());
              }),
              buildUpperMenu(HexColor("#ffffff"),
                  "assets/images/icon/whitelist.png", "Whitelist", () {
                RouteShortcut().Push(context, WhitelistForm());
              }),
              buildUpperMenu(HexColor("#ffffff"),
                  "assets/images/icon/blacklist.png", "Blacklist", () {
                RouteShortcut().Push(context, BlacklistForm());
              }),
              buildUpperMenu(HexColor("#ffffff"),
                  "assets/images/icon/report.png", "Report", () {
                RouteShortcut().Push(context, ReportV1());
              }),
            ],
          )
        ],
      ),
    );
  }

  Widget buildUpperMenu(
      HexColor cardColor, String imageAssets, String title, Function f) {
    return InkWell(
      onTap: f,
      borderRadius: BorderRadius.circular(8),
      child: Container(
        padding: EdgeInsets.only(top: 10.0),
        child: Column(
          children: <Widget>[
            Card(
              color: cardColor,
              child: Container(
                padding: EdgeInsets.all(10.5),
                child: Image.asset(
                  imageAssets,
                  width: 25,
                  height: 25,
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                  side: BorderSide(width: 0.1, color: Colors.grey)),
              elevation: 0.0,
            ),
            Container(
              padding: EdgeInsets.only(top: 5, right: 2, left: 2),
              child: Text(
                title,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: TextStyle(
                    fontSize: 10,
                    fontFamily: FontSetting.fontMain,
                    fontWeight: FontWeight.w500,
                    color: Colors.black54),
              ),
            )
          ],
        ),
      ),
    );
  }
}
