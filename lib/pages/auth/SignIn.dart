import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:snapcard/helper/color_app/color_app.dart';
import 'package:snapcard/helper/font_setting/font_setting.dart';
import 'package:snapcard/helper/reusable_widget/button.dart';
import 'package:snapcard/helper/route_shortcut/route_shortcut.dart';
import 'package:snapcard/helper/shared_preference_key/shared_preference_key.dart';
import 'package:snapcard/helper/url/url.dart';
import 'package:snapcard/pages/MainPage.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool is_process_start = false;
  SharedPreferences prefs;
  Response response;
  Dio dio = new Dio();
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();

  login() async {
    setState(() {
      is_process_start = true;
    });
    if (username.text.toString().isEmpty || password.text.toString().isEmpty) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.red[400],
        elevation: 0,
        duration: Duration(milliseconds: 2500),
        behavior: SnackBarBehavior.fixed,
        content: Text(
          "Username dan katasandi harus di isi",
          style: TextStyle(
            fontFamily: FontSetting.fontMain,
          ),
        ),
      ));
    } else {
      response = await dio.post(ApiUrl.login,
          data: FormData.fromMap({
            "username": username.text.toString().trim(),
            "password": password.text.toString().trim()
          }));
      print(response);
      if (response.data['message'] == "failed") {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          backgroundColor: Colors.red[400],
          elevation: 0,
          duration: Duration(milliseconds: 2500),
          behavior: SnackBarBehavior.fixed,
          content: Text(
            "Username atau katasandi yang kamu masukkan mungkin salah",
            style: TextStyle(
              fontFamily: FontSetting.fontMain,
            ),
          ),
        ));
      } else {
        print(response);
        prefs = await SharedPreferences.getInstance();
        prefs.setBool(SPKey.is_logged_in, true);
        prefs.setBool(SPKey.is_guest, false);
        RouteShortcut().PushReplacement(context, MainPage());
      }
    }
    setState(() {
      is_process_start = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: 'Login ',
              style: TextStyle(
                  fontFamily: FontSetting.fontMain,
                  color: Colors.black87,
                  fontWeight: FontWeight.w800,
                  fontSize: 15),
              children: <TextSpan>[
                TextSpan(
                    text: 'Snapcard',
                    style: TextStyle(
                        fontFamily: FontSetting.fontMain,
                        color: ColorApp.main_color_app,
                        fontWeight: FontWeight.w800,
                        fontSize: 15)),
              ],
            )),
      ),
      body: ListView(
        padding: EdgeInsets.only(left: 10, right: 10),
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(right: 20.0, left: 20.0),
            child: ListTile(
              contentPadding: EdgeInsets.zero,
              dense: true,
              title: Text('Halo, selamat datang kembali!',
                  style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                      color: Colors.black87,
                      fontWeight: FontWeight.w700,
                      fontSize: 18)),
              subtitle: Text('Masuk untuk melanjutkan',
                  style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal,
                      fontSize: 12)),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Username",
                  style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                      color: Colors.black87,
                      fontWeight: FontWeight.w800,
                      fontSize: 13.0),
                ),
                Container(
                    transform: Matrix4.translationValues(0.0, -8.0, 0.0),
                    child: Theme(
                        child: TextFormField(
                          controller: username,
                          keyboardType: TextInputType.text,
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          cursorColor: ColorApp.main_color_app,
                          style: TextStyle(
                              fontFamily: FontSetting.fontMain,
                              color: Colors.black54,
                              fontSize: 15),
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Username masih kosong";
                            }
                            return null;
                          },
                          obscureText: false,
                          decoration: InputDecoration(
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.black12)),
                              focusedBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: ColorApp.main_color_app)),
                              hintText: "Masukkan alamat email disini...",
                              hintStyle: TextStyle(
                                  fontFamily: FontSetting.fontMain,
                                  color: Colors.grey)),
                        ),
                        data: Theme.of(context).copyWith(
                          primaryColor: ColorApp.main_color_app,
                        ))),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Katasandi",
                  style: TextStyle(
                      fontFamily: FontSetting.fontMain,
                      color: Colors.black87,
                      fontWeight: FontWeight.w800,
                      fontSize: 13.0),
                ),
                Container(
                    transform: Matrix4.translationValues(0.0, -8.0, 0.0),
                    child: Theme(
                        child: TextFormField(
                          controller: password,
                          keyboardType: TextInputType.text,
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          cursorColor: ColorApp.main_color_app,
                          style: GoogleFonts.inter(
                              color: Colors.black54, fontSize: 15),
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Katasandi masih kosong";
                            }
                            return null;
                          },
                          obscureText: true,
                          decoration: InputDecoration(
                              enabledBorder: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.black12)),
                              focusedBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: ColorApp.main_color_app)),
                              hintText: "Masukkan katasandi disini...",
                              hintStyle: TextStyle(
                                  fontFamily: FontSetting.fontMain,
                                  color: Colors.grey)),
                        ),
                        data: Theme.of(context).copyWith(
                          primaryColor: ColorApp.main_color_app,
                        ))),
              ],
            ),
          ),
          // Container(
          //     margin: EdgeInsets.only(right: 15.0),
          //     alignment: Alignment.centerRight,
          //     child: InkWell(
          //       borderRadius: BorderRadius.circular(8),
          //       onTap: () {},
          //       child: Padding(
          //         padding: const EdgeInsets.all(10.0),
          //         child: Text(
          //           "Lupa katasandi?",
          //           style: TextStyle(
          //               color: ColorApp.main_color_app,
          //               fontFamily: FontSetting.fontMain,
          //               fontSize: 14,
          //               fontWeight: FontWeight.w700),
          //         ),
          //       ),
          //     )),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 15, right: 15, top: 5.0),
            child: PButton()
                .MainButton(is_process_start ? "Mohon tunggu" : "Masuk", () {
              login();
            }),
          ),
        ],
      ),
    );
  }
}
